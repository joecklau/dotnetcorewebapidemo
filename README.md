# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* To show how to make WebAPI in core with swagger spec

### How do I get set up? ###

* Pull, Build and Run

### Contribution guidelines ###

[Main tutorial](https://docs.microsoft.com/en-us/aspnet/core/tutorials/web-api-help-pages-using-swagger)

[XML-Comments](https://docs.microsoft.com/en-us/aspnet/core/tutorials/web-api-help-pages-using-swagger#xml-comments)

[To work with IIS when Docker support is enabled](https://stackify.com/how-to-deploy-asp-net-core-to-iis/)