﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DotNetApi.DTOs.Infrastructure;
using DotNetCoreWebApiDemo.Models;

namespace DotNetApi.DTOs
{
    public class ApplicationUserDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        
        /// <summary>
        /// A NORMALIZED email (i.e. all CAP)
        /// </summary>
        public virtual string NormalizedEmail { get; set; }
        /// <summary>
        /// Gets or sets the email address for this user.
        /// </summary>
        public virtual string Email { get; set; }
        /// <summary>
        /// Gets or sets the normalized user name for this user.
        /// </summary>
        public virtual string NormalizedUserName { get; set; }
        /// <summary>
        /// Username used to login
        /// </summary>
        public virtual string UserName { get; set; }
        /// <summary>
        /// UserID
        /// </summary>
        public virtual string Id { get; set; }

        ////ECC/ END CUSTOM CODE SECTION 

    }

    public class ApplicationUserMapper : MapperBase<ApplicationUser, ApplicationUserDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<ApplicationUser, ApplicationUserDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<ApplicationUser, ApplicationUserDTO>>)(p => new ApplicationUserDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    Id = p.Id,
                    Email = p.Email,
                    UserName = p.UserName,
                    NormalizedEmail = p.NormalizedEmail,
                    NormalizedUserName = p.NormalizedUserName,
                    ////ECC/ END CUSTOM CODE SECTION 

                }));
            }
        }

        public override void MapToModel(ApplicationUserDTO dto, ApplicationUser model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Email = dto.Email;
            model.UserName = dto.UserName;
            model.NormalizedEmail = dto.NormalizedEmail;
            model.NormalizedUserName = dto.NormalizedUserName;
            ////ECC/ END CUSTOM CODE SECTION 
        }
    }
}
