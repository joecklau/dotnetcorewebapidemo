using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using DotNetApi.DTOs;
using DotNetCoreWebApiDemo.Data;

namespace DotNetApi.Controllers
{
    /// <summary>
    /// This is a sample API controller to access user's information
    /// </summary>
    [Produces("application/json")]
    [Route("api/ApplicationUsers")]
    public class ApplicationUsersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ApplicationUsersController(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// This API List out all users' profile
        /// </summary>
        /// <returns></returns>
        // GET: api/ApplicationUsers
        [HttpGet]
        public IEnumerable<ApplicationUserDTO> GetApplicationUser()
        {
            return _context.Users.Select(new ApplicationUserMapper().SelectorExpression);
        }

        /// <summary>
        /// Extract the details of a single user
        /// </summary>
        /// <param name="id">UserID</param>
        /// <returns></returns>
        // GET: api/ApplicationUsers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApplicationUser([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var applicationUser = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);

            if (applicationUser == null)
            {
                return NotFound();
            }

            return Ok(new ApplicationUserMapper().SelectorExpression.Compile().Invoke(applicationUser));
        }

        /// <summary>
        /// Update the details of an existing user
        /// </summary>
        /// <param name="id"></param>
        /// <param name="applicationUser"></param>
        /// <returns></returns>
        // PUT: api/ApplicationUsers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutApplicationUser([FromRoute] string id, [FromBody] ApplicationUserDTO applicationUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != applicationUser.Id)
            {
                return BadRequest();
            }

            var entity =_context.Users.FirstOrDefault(x => x.Id == id);
            if (entity == null)
            {
                return NotFound();
            }
            new ApplicationUserMapper().MapToModel(applicationUser, entity);

            _context.Entry(entity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApplicationUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // TODO: It is the template-generated code, which needs modification as it is directly using the EF entity instead of DTO
        //// POST: api/ApplicationUsers
        //[HttpPost]
        //public async Task<IActionResult> PostApplicationUser([FromBody] ApplicationUser applicationUser)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    _context.Users.Add(applicationUser);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetApplicationUser", new { id = applicationUser.Id }, applicationUser);
        //}

        /// <summary>
        /// Delete a user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/ApplicationUsers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApplicationUser([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var applicationUser = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            _context.Users.Remove(applicationUser);
            await _context.SaveChangesAsync();

            return Ok(applicationUser);
        }

        private bool ApplicationUserExists(string id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}